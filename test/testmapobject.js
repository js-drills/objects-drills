
let mapobjectproblem = require('../mapobjects')

let mapObject = mapobjectproblem.mapObject
let mappingFunction = mapobjectproblem.mappingFunction


const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const mappedObject = mapObject(testObject, mappingFunction);
console.log(mappedObject)