const _ = require("lodash");

function values(objects) {
  if (typeof objects === "object") {
    let valuesArray = [];
    _.forOwn(objects, (value, key) => {
      if (typeof value !== "function") {
        valuesArray.push(value);
      }
    });
    return valuesArray;
  }
}

module.exports = values;
