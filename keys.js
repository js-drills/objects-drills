

function keys(objects) {
    if (typeof objects === 'object') {
        let keysArray = [];
        for (let key in objects) {
            if (objects.hasOwnProperty(key)) {
              keysArray.push(key);
          }
        }
       return keysArray
   }
}

module.exports = keys