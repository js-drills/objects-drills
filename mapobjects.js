const _= require('lodash')


function mapObject(objects, mappingfunction) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    if (typeof objects === 'object') {
        let mappedObject = {}
        _.forOwn(objects, (value, key) => {
            mappedObject[key] = mappingfunction(value,key,objects)
        })
        return mappedObject
    }
}

const mappingFunction = (value, key, objects) => {
    if (objects.hasOwnProperty(key)) {
        return value + " Hi!"
    }
};

module.exports = {mapObject,mappingFunction}