

function pairs(objects) {
  // Convert an object into a list of [key, value] pairs.
    if (typeof objects === 'object') {
        let keyValuePairs = []
        for (let key in objects) {
            if (objects.hasOwnProperty(key)) {
                let pair = []
                pair.push(key, objects[key])
                keyValuePairs.push(pair)
          }
        }
      return keyValuePairs;
    }
    return null;
}

module.exports = pairs