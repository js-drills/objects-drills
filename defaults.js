

function defaults(object, defaultobj) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    if (typeof object === 'object' && typeof defaultobj === 'object')
    {
        for (let key in defaultobj) {
            if (object.hasOwnProperty(key) === false) {
                object[key] = defaultobj[key] 
            }
        }
        return object
    }
    return null;
}

module.exports = defaults