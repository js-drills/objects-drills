// Assuming having unique values
function invert(obj) {
  const invertedObject = {};

  if (typeof obj === "object") {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const value = obj[key];
        invertedObject[value] = key;
      }
    }
    return invertedObject;
  }
}

module.exports = invert
